from django.forms import ModelForm

from meal_plans.models import MealPlan

# meal plan form
# meal plan delete form


class MealPlanForm(ModelForm):
    class Meta:
        model = MealPlan
        fields = ["name", "recipes", "date"]


class MealPlanDeleteForm(ModelForm):
    class Meta:
        model = MealPlan
        fields = []
