from django.urls import path

from meal_plans.views import (
    MealPlansListView,
    MealPlansCreateView,
    MealPlansDetailView,
    MealPlansEditView,
    MealPlansDeleteView,
)

urlpatterns = [
    path("", MealPlansListView.as_view(), name="meal_plans"),
    path("new/", MealPlansCreateView.as_view(), name="meal_plan_new"),
    path("<int:pk>", MealPlansDetailView.as_view(), name="meal_plan_detail"),
    path("<int:pk>/edit", MealPlansEditView.as_view(), name="meal_plan_edit"),
    path(
        "<int:pk>/delete",
        MealPlansDeleteView.as_view(),
        name="meal_plan_delete",
    ),
]
